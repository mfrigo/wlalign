.. _installation:

============
Installation
============
WL-align runs only on Python 3.
The installation has the following dependencies:

* Numpy
* Scipy

If you are an Anaconda user, you may want to create a dedicated ``wlalign-env``
environment and populate it with the right dependencies, then install WL-align.

.. code:: bash

    conda env create -n wlalign-env -f environment.yml
    pip install wlalign

Alternatively, you can install the dependencies and WL-align all via ``pip``.

.. code:: bash

    pip install numpy
    pip install scipy

    pip install wlalign

To install WL-align directly from the source, clone this repository and run the
standard local setup commands.

.. code:: bash

    git clone https://gitlab.inria.fr/cobcom/wlalign.git
    cd wlalign
    pip install -U .


Check installation
==================
To check that WL-align has been properly installed, try to import the ``wlalign``
module into a Python session and print the version as follows. If no error is
raised, the installation has been successful.

.. code:: python

    >>> import wlalign
    >>> print(wlalign.__version__)


For developers
==============
If you are thinking about developing your own fork of WL-align, you may want to
use the latest version in the ``devel`` branch of the repository and install it
in editable mode.

.. code:: bash

    git clone https://gitlab.inria.fr/cobcom/wlalign.git
    cd WL-align
    git checkout devel
    pip install -e .


Tests
~~~~~
The package uses ``unittest`` as a testing suite.
To run all the tests, execute the following command in the source's root
directory.

.. code:: bash

    python -m unittest -v

Test coverage can be checked with ``coverage`` as follows.

.. code:: bash

    coverage run -m unittest
    coverage report -m


Documentation
~~~~~~~~~~~~~
The sources of the documentation are in the ``doc`` folder.
The compilation requires the ``sphinx`` package and the theme to be installed.

.. code:: bash

    pip install sphinx
    pip install sphinx_rtd_theme

To compile the documentation, move to the ``doc`` folder and run
``make <format>``, where the format can be ``html``, ``latex`` or any other
sphinx-compatible format.
To get a local copy of the the html documentation, run the ``make html``
command.

.. code:: bash

    cd doc
    make clean  # deletes results of previous compilations
    make html

