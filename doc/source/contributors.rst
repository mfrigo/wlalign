.. _contributors:

====================
List of Contributors
====================
WL-align was conceived in the COATI and ATHENA Project Teams at Inria Sophia
Antipolis - Méditerranée. The Python package was developed by:

* `Matteo Frigo <https://www.mfrigo.com>`_ , ATHENA Project Team, Inria Sophia
    Antipolis - Méditerranée.
* `Emilio Cruciani <https://sites.google.com/view/emiliocruciani/>`_ , COATI
    Project Team, Inria Sophia Antipolis - Méditerranée.
