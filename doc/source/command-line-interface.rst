.. _command-line-interface:

===========================
CLI: Command Line Interface
===========================

The user can align two graphs from the command line by providing their
adjacency matrices in text form.
The edge weights in each line must be separated by whitespaces.

.. code:: bash

    wlalign --help

.. code:: text

    usage: wlalign [-h] [--first_aligned FIRST_ALIGNED] [--k INT] [--l INT]
                   [--force] [-v]
                   in_graph1 in_graph2 out_matching

    This program uses WL-align to compute an alignment between two graphs having
    the same number of nodes. It takes as input their adjacency matrices and it
    returns a matching (a.k.a. alignment) between their nodes.

    positional arguments:
      in_graph1             Path to the first graph to be aligned
      in_graph2             Path to the second graph to be aligned
      out_matching          Path where the matching will be saved. The first
                            element of each row is the index of the node in the
                            first graph that is aligned with the node in the
                            second graph indexed by the second element of the row

    optional arguments:
      -h, --help            show this help message and exit
      --first_aligned FIRST_ALIGNED
                            The aligned version of the first input graph will be
                            saved in the specified position (default: None)
      --k INT               Width parameter (default: 2)
      --l INT               Depth parameter (default: 2)
      --force               Overwrite existing files (default: False)
      -v, --verbose         Set verbose output (default: False)
