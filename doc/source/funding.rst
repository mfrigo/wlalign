.. _funding:

====================
Funding
====================
The development of WL-align was funded by the European Research Council (ERC)
under the European Union’s Horizon 2020 research and innovation program (ERC
Advanced Grant agreement No 694665: `CoBCoM - Computational Brain Connectivity
Mapping <https://project.inria.fr/cobcom/>`_ ).

.. image:: img/logo_erc_eu.jpg
    :align: center
    :width: 600
