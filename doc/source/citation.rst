.. _citation:

====================
How to cite WL-align
====================
If you use WL-align in your research, please cite the following article.

    Matteo Frigo, Emilio Cruciani, David Coudert, Rachid Deriche, Emanuele
    Natale, Samuel Deslauriers-Gauthier; Network alignment and similarity
    reveal atlas-based topological differences in structural connectomes.
    Network Neuroscience 2021;
    `DOI: 10.1162/netn_a_00199 <https://doi.org/10.1162/netn_a_00199>`_

.. code:: bibtex

    @article{wlalign,
          author = {Frigo, Matteo and Cruciani, Emilio and Coudert, David and
                    Deriche, Rachid and Natale, Emanuele and
                    Deslauriers-Gauthier, Samuel},
          title = {Network alignment and similarity
                   reveal atlas-based topological differences in structural
                   connectomes},
          journal = {Network Neuroscience},
          url = {https://hal.archives-ouvertes.fr/hal-03116143},
          doi = {10.1162/netn_a_00199},
          year = {2021}
         }
