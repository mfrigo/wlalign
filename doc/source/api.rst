.. _api:

================
Alignment module
================

.. automodule:: wlalign.alignment
    :members: permutation_from_alignment, apply_alignment, length_wl_signature, signature_wl, wl_align

=================
Similarity module
=================

.. automodule:: wlalign.similarity
    :members: graph_jaccard_index

============
Utils module
============

.. automodule:: wlalign.utils
    :members: load_network, symmetrize_adj, check_is_adj, check_compatible_adj, check_is_alignment, check_can_write_file
